/* execution command: LD_PRELOAD=$PWD/inject.so ./test */

#include <stdio.h>
#include <unistd.h>

int main(){

    if (fork() == -1) 
        printf("stopped\n"); 
    else{
        printf("fork should stop!\n");
    }

    return 0;
}