#ifndef INJECT_H_
#define INJECT_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <errno.h>
#include <syscall.h>

#define MAX_CAPACITY 300

#if __WORDSIZE == 64
#define REG(reg) reg.orig_rax
#define SET_REG(reg,num) reg.rax = num;
#define SET_ORIG_REG(reg,num) reg.orig_rax = num;
#else
#define REG(reg) reg.orig_eax
#define SET_REG(reg,num) reg.eax = num;
#define SET_ORIG_REG(reg,num) reg.orig_eax = num;
#endif

const char* callname(long call);

#endif