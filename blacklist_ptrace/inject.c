/* For reference, system call numbers can be found in /usr/include/asm/unistd.h*/

#include "inject.h"

int blacklist_includes(int code, int *blacklist){

    int i = 0;
    while(blacklist[i] != -1){
        if(blacklist[i] == code){
            return 1;
        }
        i++;
    }
    return 0;
}

/* insert code to invoked systemcalls */
int already_invoked(int code, int *invoked_syscalls, int *size){

    int i = 0;
    /*check if already called*/
    while(invoked_syscalls[i] != -1){
        if(code == invoked_syscalls[i]) return 1; //true
        i++;
    }
    /*else insert*/
    invoked_syscalls[*size] = code;
    (*size)++;
    invoked_syscalls[*size] = -1; //like a sentinel

    return 0;
}

int main(int argc, char* argv[]){

    /* for the statistics */
    int prohibited = 0, unique = 0, total = 0;

    int enter_syscall = 1;

    /* stores every unique syscall the program executed */
    int invoked_syscalls[MAX_CAPACITY];

    invoked_syscalls[unique] = -1;

    pid_t pid;
    FILE *fp;
    fp = fopen("blacklist.txt", "r");

    //read file into array
    int *blacklist;
    int j, i = 0;

    blacklist = (int*)malloc(sizeof(int));

    while(fscanf(fp, "%d", &blacklist[i]) == 1){
        i++;
        blacklist = (int*)realloc(blacklist, (i+1)*sizeof(int));
    }
    blacklist[i] = -1;

    char *args[]={"./test",NULL};
    long int code; /* syscall code (%eax value) */

    pid = fork();
    if(pid == 0) {
        ptrace(PTRACE_TRACEME, 0, NULL, NULL);
        execvp(args[0],args);
    } else {
        int status;

        while(waitpid(pid, &status, 0) && ! WIFEXITED(status)){

           /* inspect the registers */
            struct user_regs_struct regs;
            ptrace(PTRACE_GETREGS, pid, NULL, &regs);

            /* Is this system call permitted? */
            int blocked = 0;
            code = REG(regs);

            if(enter_syscall == 1){
                total++;
                already_invoked(code, invoked_syscalls, &unique);
                fprintf(stderr, "try system call %ld (%s) pid %d\n", code, callname(code), pid);
                enter_syscall = 0;
            }else
                enter_syscall = 1;

            if(blacklist_includes(code, blacklist)){
                printf("system call %ld (%s) blocked\n",code, callname(code));
                blocked = 1;
                SET_ORIG_REG(regs,-1); // set to invalid syscall
                ptrace(PTRACE_SETREGS, pid, 0, &regs);
                prohibited++;
                total--;
            }
            /* Run system call and stop on exit */
            ptrace(PTRACE_SYSCALL, pid, 0, 0);

            if (blocked) {
              /* errno = EPERM */
              SET_REG(regs,-EPERM); // Operation not permitted
              ptrace(PTRACE_SETREGS, pid, 0, &regs);
            }
        }
    }

    free(blacklist);
    printf("exiting...\n");
    printf("\nProhibited = %d\n", prohibited);
    printf("Unique = %d\n", unique);
    printf("Total syscalls = %d\n", total);
    return 0;
}