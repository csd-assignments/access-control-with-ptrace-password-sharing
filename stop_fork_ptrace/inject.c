/* tracer: executes the executable produced by test.c
    prohibits the execution of fork() */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <errno.h>
#include <syscall.h>

#if __WORDSIZE == 64
#define REG(reg) reg.orig_rax
#define SET_REG(reg,num) reg.rax = num;
#define SET_ORIG_REG(reg,num) reg.orig_rax = num;
#else
#define REG(reg) reg.orig_eax
#define SET_REG(reg,num) reg.eax = num;
#define SET_ORIG_REG(reg,num) reg.orig_eax = num;
#endif

int main(int argc, char* argv[]){

    pid_t pid;

    char *args[]={"./test",NULL};
    long int code; /* syscall code (%eax value) */

    pid = fork();
    if(pid == 0) {
        ptrace(PTRACE_TRACEME, 0, NULL, NULL);
        execvp(args[0],args);
    } else {
        int status;

        while(waitpid(pid, &status, 0) && ! WIFEXITED(status)){

           /* inspect the registers */
            struct user_regs_struct regs;
            ptrace(PTRACE_GETREGS, pid, NULL, &regs);

            /* Is this system call permitted? */
            int blocked = 0;
            code = REG(regs);

            if(code == __NR_clone){
                printf("system call %ld blocked\n",code);
                blocked = 1;
                SET_ORIG_REG(regs,-1); // set to invalid syscall
                ptrace(PTRACE_SETREGS, pid, 0, &regs);
            }
            /* Run system call and stop on exit */
            ptrace(PTRACE_SYSCALL, pid, 0, 0);

            if (blocked) {
              /* errno = EPERM */
              SET_REG(regs,-EPERM); // Operation not permitted
              ptrace(PTRACE_SETREGS, pid, 0, &regs);
            }
        }
    }
    printf("exiting...\n");
    return 0;
}