/* traced: produce an executable to be traced by inject.c */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/reg.h>

int main(){

    pid_t pid;

    printf("################ I AM THE CHILD CODE!\n\n");

    pid = fork();
    if(pid == 0){
        execl("/bin/date","date",NULL);
    }else if(pid > 0){
        wait(NULL);
    }else{
        printf("fork error\n");
    }
    
    return 0;
}