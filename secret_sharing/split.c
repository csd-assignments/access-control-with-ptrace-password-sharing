#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define MAX 50

/* a: coefficient, i: independent value, piece = f(i) */
double polynomial(double *a, double i){

    int k;
    double piece = 0;
    for(k=0; k<9; k++){
        piece += a[k]*pow(i,(double)k);
    }
    return piece;
}

int main(int argc, char* argv[]){

    if(argc != 2){
        printf("You must give a password!\n");
        return 0;
    }
    int password = atoi(argv[1]);
    //printf("password is %d\n", password);
    int i;
    double a[9],f[10]; //make it double to avoid overflow

    if(password < 0){
        printf("Password must be a positive integer!\n");
        return 0;
    }

    a[0] = password;
    printf("random coefficients a[i]:\n");
    printf("%f  ", a[0]);

    srand(time(NULL));

    for(i=1; i<9; i++){
        a[i] = rand()%MAX; /* random int between 0 and MAX-1 */
        printf("%f  ", a[i]);
    }
    
    printf("\n\n( i, f(i) ) tuples:\n\n");

    for(i=1; i<=10; i++){
        f[i-1] = polynomial(a, (double)i);
        printf("(%d, %f)\n",i, f[i-1]);
    }

    return 0;
}