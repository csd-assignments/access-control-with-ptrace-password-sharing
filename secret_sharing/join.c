//Gauss Elimination
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 9 //equations

int main(int argc, char *argv[]){

    if(argc < 19){ /* executable + i + f(i) */
        printf("Points (i,f(i)) must be at least 9.\n");
        return 0;
    }
    int i,j,k;
    int numbers[9]; //the first 9 x coordinates
    double f[9]; //the first 9 y coordinates
    long double a[N][N+1], x[N]; //the elements of augmented-matrix 

    /* retrieve 1 f(1) 2 f(2) ... 9 f(9) */
    k = 1;
    for(i=0; i<9; i++){
        numbers[i] = atoi(argv[k++]);
        f[i] = atoi(argv[k++]);
        //printf("%d %f  ",numbers[i], f[i]);
    }

    printf("\nThe elements of the augmented-matrix:\n");
    for (i=0;i<N;i++){
        for (j=0;j<N;j++){
            a[i][j] = (long double)pow(numbers[i],j);
            printf("%Lf  ", a[i][j]);
        }
        a[i][j] = (long double)f[i];
        printf("%Lf  \n", a[i][j]);
    }

    for (i=0;i<N;i++) { //Pivotisation
        for (k=i+1;k<N;k++){

            if (abs(a[i][i]) < abs(a[k][i])){
                for (j=0;j<=N;j++){
                    long double temp = a[i][j];
                    a[i][j] = a[k][j];
                    a[k][j] = temp;
                }
            }
        }
    }
    printf("\nThe matrix after Pivotisation is:\n");
    for (i=0;i<N;i++) {
        for (j=0;j<=N;j++)
            printf("%Lf  ",a[i][j]);

        printf("\n");
    }

    for (i=0;i<N-1;i++){ //loop to perform the gauss elimination
        for (k=i+1;k<N;k++){
            long double t = a[k][i]/a[i][i];
            for (j=0;j<=N;j++){
            /*make the elements below the pivot elements equal to zero or elimnate the variables*/
                a[k][j] = a[k][j] - t*a[i][j];
            }
        }
    }
    printf("\n\nThe matrix after gauss-elimination is as follows:\n");
    for (i=0;i<N;i++){
        for (j=0;j<=N;j++)
            printf("%Lf  ",a[i][j]);
        printf("\n");
    }

    for (i=N-1;i>=0;i--){  //back-substitution
    //x is an array whose values correspond to the values of a0,a1,a2..
        x[i] = a[i][N]; //make the variable to be calculated equal to the rhs of the last equation
        for (j=i+1;j<N;j++)
            if (j!=i)   //then subtract all the lhs values except the coefficient of the variable whose value is being calculated
                x[i] = x[i]-a[i][j]*x[j];
        x[i] = x[i]/a[i][i];  //now finally divide the rhs by the coefficient of the variable to be calculated
    }
    printf("\nThe values of the variables are as follows:\n");
    for (i=0;i<N;i++)
        printf("%Lf\n",x[i]);  // Print the values of a0,a1,a2...  
    
    printf("\nThe password is: %Lf\n",x[0]);
    return 0;
}